<?php

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'twc-large-thumb' );
$url = $thumb['0'];

$cat_args = array(
  'orderby' => 'name',
  'order' => 'ASC'
);

$cats = get_categories($cat_args);

$twitter_link = 'text=' . urlencode(get_field('twitter_message')) . '&url=' . get_permalink() . '&via=weatherchannel';

?>

<article class="tease tease--chapter">
  <div class="tease--cell tease--cell--left">
    <a href="<?php the_permalink(); ?>">
      <img class="lazyload tease--thumb__img" data-src="<?php echo $url ?>" alt="<?php the_title(); ?>" src="<?php echo get_template_directory_uri(); ?>/assets/img/util/placeholder-tease-thumb.png">
    </a>
  </div>
  <div class="tease--cell tease--cell--right">
    <div class="tease--content">
      <p class="tease--categories">
        <?php the_category(' | '); ?>
      <h4 class="tease--title"><?php the_title(); ?></h4>
      <p class="meta--light tease--byline"><?php the_field('header_byline'); ?></p>
      <p><?php the_field('header_summary', false, false); ?></p>
    </div>
    <footer class="tease--footer">
      <div class="tease--share__wrap">
        <ul class="share">
          <li>
              <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>&title=<?php the_title(); ?>&summary=<?php the_field('header_summary') ?>&source=<?php wp_title() ?>" target="blank">
                <svg viewBox="0 0 100 100" class="icon icon-linkedin">
                  <use xlink:href="#shape-linkedin"></use>
                </svg>
              </a>
          </li>
          <li>
              <a href="https://twitter.com/intent/tweet?<?php echo htmlentities($twitter_link) ?>" target="blank">
                <svg viewBox="0 0 100 100" class="icon icon-twitter">
                  <use xlink:href="#shape-twitter"></use>
                </svg>
              </a>
          </li>
          <li>
              <a href="http://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="blank">
                <svg viewBox="0 0 100 100" class="icon icon-facebook">
                  <use xlink:href="#shape-facebook"></use>
                </svg>
              </a>
          </li>
        </ul>

      </div>
      <div class="tease--more__wrap">
        <a href="<?php the_permalink() ?>" class="btn btn--rotate tease--excerpt__more">Read Article &rarr;</a>
      </div>
    </footer>
  </div>
</article>


<!--
{% extends "teases/tease.twig" %}

{% block content %}

    <div class="tease--cell {{loop.index is divisible by(2) ? ' tease--cell--left' : ' tease--cell--right'}}">
      <a href="{{post.link}}">
        <img class="lazyload tease--thumb__img" data-src="{{post.thumbnail.src|resize(400, 300)}}" alt="{{post.post_title}}" src="{{site.theme.link}}/assets/img/util/placeholder-tease-thumb.png">
      </a>
    </div>
    <div class="tease--cell{{loop.index is divisible by(2) ? ' tease--cell--right' : ' tease--cell--left'}}">
      <div class="tease--content">
        {% if post.get_terms('category') %}
          <p class="tease--categories">
          {% for c in post.get_terms('category') %}
            <a href="{{c.link}}" class="tease--categories__link">{{c.title}}</a>{{loop.last ? ' ' : ' | '}}
          {% endfor %}
          </p>
        {% endif %}
        <h4 class="tease--title">{{post.post_title}}</h4>
        <p class="meta--light tease--byline">{{post.header_byline}}</p>
        <p>{{post.header_summary}}</p>
      </div>
      <footer class="tease--footer">
        <div class="tease--share__wrap">
          {% include 'utility/share-btns.twig' %}
        </div>
        <div class="tease--more__wrap">
          <a href="{{post.link}}" class="btn btn--rotate tease--excerpt__more">Read Article &rarr;</a>
        </div>
      </footer>
    </div>

{% endblock %}
 -->
