<?php

/*
 *
 * Menus
 *
 */

// Again, same as with taxonomies and CPTs. You just need the arguments and register_nav_menus() function here.

// Register Navigation Menus

$locations = array(
  'footer_main' => 'Footer Menu',
  'menu_main' => 'Main Menu'
);
register_nav_menus( $locations );
