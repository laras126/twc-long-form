<?php

if (!class_exists('Timber')){
  add_action( 'admin_notices', function(){
    echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . admin_url('plugins.php#timber') . '">' . admin_url('plugins.php') . '</a></p></div>';
  });
  return;
}

class StarterSite extends TimberSite {

  function __construct(){
    add_theme_support('post-formats');
    add_theme_support('post-thumbnails');
    add_theme_support('menus');
    add_filter('timber_context', array($this, 'add_to_context'));
    add_filter('get_twig', array($this, 'add_to_twig'));
    add_action('init', array($this, 'register_post_types'));
    add_action('init', array($this, 'register_menus'));
    add_action('init', array($this, 'register_taxonomies'));
    add_action('init', array($this, 'twc_acf_utils'));
    parent::__construct();
  }

  // Note that the following included files only need to contain the Taxonomy/CPT/Menu arguments and register_whatever function. They are initialized here.
  // http://generatewp.com is nice

  function register_post_types(){
    require('lib/custom-types.php');
  }

  function register_taxonomies(){
    require('lib/taxonomies.php');
  }

  function register_menus() {
    require('lib/menus.php');
  }

  function twc_acf_utils() {
    require('lib/acf-utils.php');
  }

  function add_to_context($context){

    $context['menu'] = new TimberMenu('menu_main');
    $context['footer_menu_main'] = new TimberMenu('footer_main');
    $context['footer_menu_bottom'] = new TimberMenu('footer_bottom');
    $context['site'] = $this;

    global $post;

    /*
     *
     * Site-wide Settings Context
     *
     */

    if(wp_is_mobile()) {
      $context['is_mobile'] = true;
    }

    // Footer colophon - not really using

    $context['site_footer_copyright'] = get_field('site_footer_copyright', 'options');
    $context['site_footer_credits'] = get_field('site_footer_credits', 'options');

    // Header
    $context['logo_left'] = get_field('site_left_logo', 'options');
    $context['logo_right'] = get_field('site_right_logo', 'options');
    $context['logo_right_link'] = get_field('site_right_logo_link', 'options');
    $context['twitter_link'] = get_field('twitter_link', 'options');
    $context['facebook_link'] = get_field('facebook_link', 'options');

    // Homepage
    $context['thumb_cols'] = get_field('homepage_columns', 'options');

    // KR Pano Check
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    $context['krpano'] = is_plugin_active('wp-pano');

    // Favicon
    $context['favicon_16'] = get_field('favicon_16', 'options');
    $context['favicon_32'] = get_field('favicon_32', 'options');

    $chapter_args = array(
      'post_type' => 'chapter',
      'meta_query' => array(
        'relation' => 'AND',
        array (
          'key' => 'show_in_chapter_feed',
            'value' => 1,
            'compare' => '='
        )
      )
    );

    $context['selected_chapters'] = Timber::get_posts($chapter_args);

    return $context;
  }

  function add_to_twig($twig){
    /* this is where you can add your own fuctions to twig */
    $twig->addExtension(new Twig_Extension_StringLoader());
    return $twig;
  }

}

new StarterSite();



/*
 **************************
 * Custom Theme Functions *
 **************************
 *
 * Namespaced "twc" - find and replace with your own three-letter-thing.
 *
 */

// Enqueue scripts
function twc_scripts() {

  // Use jQuery from CDN, enqueue in footer
  // Add Picturefill
  if (!is_admin()) {
    wp_deregister_script('jquery');

    wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js', array(), null, true);
    wp_register_script('head-scripts', get_template_directory_uri() . '/assets/js/build/scripts-head.min.js', array(), null);

    wp_enqueue_script('head-scripts');
  }

  // Enqueue stylesheet and scripts. Use minified for production.
  // if( WP_ENV == 'production' ) {
  if( WP_ENV == 'development' ) {
    wp_enqueue_style( 'twc-styles', get_template_directory_uri() . '/assets/css/build/main.min.css', 1.0);
    wp_enqueue_script( 'twc-js', get_template_directory_uri() . '/assets/js/build/scripts.js', array('jquery'), '1.0.0', true );
  } else {
    wp_enqueue_style( 'twc-styles', get_stylesheet_directory_uri() . '/assets/css/build/main.css', 1.0);
    wp_enqueue_script( 'twc-js', get_template_directory_uri() . '/assets/js/build/scripts.js', array('jquery'), '1.0.0', true );
  }
}
add_action( 'wp_enqueue_scripts', 'twc_scripts' );


// Remove slug from Chapter custom type. This seems too hacky, but okay for now?
// http://www.markwarddesign.com/2014/02/remove-custom-post-type-slug-permalink/

function twc_remove_cpt_slug( $post_link, $post, $leavename ) {

    if ( ! in_array( $post->post_type, array( 'chapter' ) ) || 'publish' != $post->post_status )
        return $post_link;

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}
add_filter( 'post_type_link', 'twc_remove_cpt_slug', 10, 3 );

function twc_parse_request_tricksy( $query ) {

    // Only noop the main query
    if ( ! $query->is_main_query() )
        return;

    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query )
        || ! isset( $query->query['page'] ) )
        return;

    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if ( ! empty( $query->query['name'] ) )
        $query->set( 'post_type', array( 'post', 'chapter', 'page' ) );
}
add_action( 'pre_get_posts', 'twc_parse_request_tricksy' );


// Save par value into cookie and expire after closing browser window

function twc_save_cookie() {
  $cookie_name = "par";
  if (!is_admin() && isset($_GET["par"])) {
    $cookie_value = $_GET["par"];
    setcookie($cookie_name, $cookie_value, time() + 3600, "/");
  } else {
  setcookie($cookie_name, "", time() -3600, "/");
  }
}
add_action('init', 'twc_save_cookie');



/*
 *
 * Nice to Haves
 *
 */


// Add body class if is mobile device
add_filter('body_class','twc_body_class');
function twc_body_class($classes) {
  if(wp_is_mobile()) {
    $classes[] = 'twc-is-mobile';
  }
  return $classes;
};


// Change Title field placeholders for Custom Post Types
// (You'll need to register the types, of course)

function twc_title_placeholder_text ( $title ) {
  if ( get_post_type() == 'chapter' ) {
    $title = __( 'Chapter Title' );
  }
  return $title;
}
add_filter( 'enter_title_here', 'twc_title_placeholder_text' );



// Customize the editor style
// It's just the Bootstrap typography, but I like it. Got the idea from Roots.io.

function twc_editor_styles() {
  add_editor_style( 'assets/css/editor-style.css' );
}
add_action( 'after_setup_theme', 'twc_editor_styles' );



// Add excerpts to pages
function twc_add_excerpts_to_pages() {
  add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'twc_add_excerpts_to_pages' );



// Remove inline gallery styles
add_filter( 'use_default_gallery_style', '__return_false' );



// Hide Customizr completely
// Credit: https://wordpress.org/support/topic/feature-request-one-line-of-code-to-disable-all-customizer-stuff

function shmoo_customize() {
  // Disallow acces to an empty editor
  wp_die( sprintf( __( 'No WordPress Theme Customizer support - If needed check your functions.php' ) ) . sprintf( '<br /><a href="javascript:history.go(-1);">Go back</a>' ) );
}
add_action( 'load-customize.php', 'shmoo_customize' );

// Remove 'Customize' from Admin menu
function remove_submenus() {
  global $submenu;
  // Appearance Menu
  unset($submenu['themes.php'][6]); // Customize
}
add_action('admin_menu', 'remove_submenus');

// Remove 'Customize' from the Toolbar -front-end
function remove_admin_bar_links() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('customize');
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

// Add Custom CSS to Back-end head
function shmoo_admin_css() {
  echo '<style type="text/css">#customize-current-theme-link { display:none; } </style>';
}
add_action('admin_head', 'shmoo_admin_css');



// Add large Thumbnail size
add_image_size( 'twc-large-thumb', 400, 400 );


// Load get_template_part into a variable to abstract markup from response
// Credit: http://wordpress.stackexchange.com/questions/171979/loading-page-content-into-a-variable-in-template
function return_get_template_part($slug, $name=null) {

  ob_start();
  get_template_part($slug, $name);
  $content = ob_get_contents();
  ob_end_clean();

  return $content;
}

// Include custom post types in category archives
function twc_add_custom_types( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
     'chapter'
    ));
    return $query;
  }
}
add_filter( 'pre_get_posts', 'twc_add_custom_types' );

// *******
// Ajax posts filter by category
// Credit: http://www.bobz.co/ajax-filter-posts-tag/
// ******

// Enqueue script
function ajax_filter_posts_scripts() {

  // Enqueue script
  wp_register_script('afp_script', get_template_directory_uri() . '/assets/js/_ajax-request.js', false, null, false);
  wp_enqueue_script('afp_script');

  wp_localize_script( 'afp_script', 'afp_vars', array(
        'afp_nonce' => wp_create_nonce( 'afp_nonce' ), // Create nonce which we later will use to verify AJAX request
        'afp_ajax_url' => admin_url( 'admin-ajax.php' ),
      )
  );
}
add_action('wp_enqueue_scripts', 'ajax_filter_posts_scripts', 100);

$result = array();

// Script for getting posts
function ajax_filter_get_posts( $taxonomy ) {

  // Verify nonce
  if( !isset( $_POST['afp_nonce'] ) || !wp_verify_nonce( $_POST['afp_nonce'], 'afp_nonce' ) )
    die('Permission denied');

  $taxonomy = $_POST['taxonomy'];

  $args = array(
    'post_type' => 'chapter',
    'category_name'  => $taxonomy,
    'meta_query' => array(
      'relation' => 'AND',
      array (
        'key' => 'show_in_chapter_feed',
          'value' => 1,
          'compare' => '='
      )
    )
  );

  // If taxonomy is not set, remove key from array and get all posts
  if( !$taxonomy ) {
    unset( $args['category_name'] );
  }

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

    $result['response'][] = return_get_template_part('lib/ajax-tease', 'chapter');

    $result['status']     = 'success';

  endwhile; else:
    $result['response'] = '<h2>No posts found</h2>';
    $result['status']   = '404';
  endif;

  $result = json_encode($result);
  echo $result;

  die();
}

add_action('wp_ajax_filter_posts', 'ajax_filter_get_posts');
add_action('wp_ajax_nopriv_filter_posts', 'ajax_filter_get_posts');
