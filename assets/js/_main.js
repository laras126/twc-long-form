
$(document).ready(function() {

    // ----
    // Toggle Menu
    // ----

    // TODO: better fallback for non-JS - adding a .js class but it causes the nav to blink
    // Look into Modernizr for that

    var $menu = $('#menu'),
        $menulink = $('.menu-link');

    $menulink.on('click', function(e) {
      e.preventDefault();
      $menulink.toggleClass('active');
      $menu.toggleClass('active');
      return false;
    });


    // Close menu on body click and escape - this is spaghetti as well.
    $('body').on( 'click', function(){
      if ($menulink.hasClass('active')) {
        $menulink.removeClass('active');
        $menu.removeClass('active');
      }
    });

    $(document).on( 'keyup', function(e) {
      if (e.keyCode == 27) {
        if ($menu.hasClass('active')) {
            $menu.removeClass('active');
        }
      }
    });


    // ----
    // Iframe Requests on Click
    // ----

    $('.vr-container').on('click', function(e) {
        var iframe_src = $(this).attr('data-vr-src');
        $(this).find('.iframe-trigger').hide();
        $(this).find('.vr-icon').hide();
        $(this).find('.iframe-target iframe').attr('src', iframe_src);
        $(this).find('.iframe-target').show();
    });

    $('.watch-btn').on('click', function(e) {
        var iframe_src = $(this).attr('data-iframe-src');
        $('.watch-btn-target iframe').attr('src', iframe_src);
        $('.watch-btn-target').animate({'opacity':1}, 200);
        $('.header--single__hero__img__wrap')
          .addClass('hide-for-watch')
          .animate({
            'opacity':0
          }, 200, function() {
            $(this).children().remove();
            $(this).animate({
              'opacity':1
              }, 0)
          });

        return false;
    });




    // ----
    // Plugins
    // ----

    $('.site__main').fitVids({ ignore: '.block_ad, .watch-btn-target' });

    $('.header__hero--slider').flickity({
        autoPlay: 9000,
        wrapAround: true
    });

    $('.loop__tease--thumb').flickity({
        initialIndex: 1,
        wrapAround: true,
        imagesLoaded: true,
        autoPlay: 1500
    });

    $('.block_gallery__gallery').flickity({
        initialIndex: 1,
        wrapAround: true,
        imagesLoaded: true
    });

    window.lazySizesConfig = {
        addClasses: true
    };

    // Reveal animations when in view
    new WOW().init();


    // ----
    // Smoothly Load Background Videos
    // ---

    // If there are videos, load them nicely
    // Thx: http://atomicrobotdesign.com/blog/web-development/check-when-an-html5-video-has-loaded/

    if( $('video').length ) {
      window.addEventListener('load', function() {
        var video = document.querySelector('.ambient__video');
        var preloader = document.querySelector('.spinner');

        function checkLoad() {
          if (video.readyState === 4) {
            video.play();
          } else {
            setTimeout(checkLoad, 100);
          }
        }
        checkLoad();
      }, false);
    }


    // ----
    // Only play videos when in viewport
    // ----

    // http://stackoverflow.com/questions/15395920/play-html5-video-when-scrolled-to
    var videos = document.getElementsByTagName("video"),
    fraction = 0.2;

    var checkScroll = debounce(function() {

      for(var i = 0; i < videos.length; i++) {

        var video = videos[i];

        var x = video.offsetLeft,
            y = video.offsetTop,
            w = video.offsetWidth,
            h = video.offsetHeight,
            r = x + w, // right
            b = y + h, // bottom
            visibleX, visibleY, visible;

            visibleX = Math.max(0, Math.min(w, window.pageXOffset + window.innerWidth - x, r - window.pageXOffset));
            visibleY = Math.max(0, Math.min(h, window.pageYOffset + window.innerHeight - y, b - window.pageYOffset));

            visible = visibleX * visibleY / (w * h);

            if (visible > fraction) {
              video.play();
            } else {
              video.currentTime = 0;
              video.pause();
            }
      }
    });

    window.addEventListener('scroll', debounce(checkScroll), false);
    window.addEventListener('resize', debounce(checkScroll), false);

    // https://davidwalsh.name/javascript-debounce-function
    function debounce(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    };





    // ----
    // Scroll to Top link
    // ----

    $('.footer--site__nav--backtop a').on( 'click', function() {
        var hash = $('#pageTop');
        var $target = $(hash);

        // Slide to section corresponding to clicked hash
        $('html,body').animate({
            scrollTop: $target.offset().top
        }, 700);

        return false;

    });

    // Fade in backtop link when the header is no longer visible
    $(window).scroll( function() {
        var headerHeight = $('.header').outerHeight();

        if ( $(this).scrollTop() > headerHeight ) {
            $('.footer--site__nav--backtop a').fadeIn(400);
        } else if ($(this).scrollTop() < headerHeight) {
            $('.footer--site__nav--backtop a').fadeOut(400);
        }
    });




    // ----
    // Before and After Slider (Image Comparison Block)
    // ----

    // Without the animations
    // codyhouse.co/gem/css-jquery-image-comparison-slider/

    // Make the .cd-handle element draggable and modify .cd-resize-img width according to its position
    $('.cd-image-container').each(function(){
        var actual = $(this);
        drags(actual.find('.cd-handle'), actual.find('.cd-resize-img'), actual, actual.find('.cd-image-label[data-type="original"]'), actual.find('.cd-image-label[data-type="modified"]'));
    });




    // ----
    // Chapter Nav
    // ----

    // This jumps...will need to fix
     var scroll_class = 'stuck',
      $nav = $('.nav--single'),
      nav_ht = $nav.outerHeight(),
      top_nav_ht = $('.header--site').outerHeight(),
      nav_total_ht = nav_ht + top_nav_ht;
      header_ht = $('.header--single').outerHeight() - nav_total_ht - $('.header--single__hero__info').outerHeight();


    // 1. Highlight current item
    // 2. Slide to current section on click
    $('.nav--single__item a').on('click', function(e) {

      var hash = $(this).attr('href');
      var $target = $('.chapter-nav-target' + hash);

      // Mark clicked item
      // $('.nav--single__item a').removeClass('-current');
      //$(this).addClass('-current').delay(1500).removeClass('-current');

      // Slide to section corresponding to clicked hash
      var target_loc = $target.offset().top - nav_total_ht;
      $('html,body').animate({
        scrollTop: target_loc.toFixed(0)
      }, 700);
      console.log(target_loc.toFixed(0));
      e.preventDefault();

    }); // END click

     // if($('.nav--single__item a').hasClass('-current')) {
       // Hacky way to reset current Chapter indicator
       // setTimeout(function() {
       // }, 2000);
     // }

    $(window).scroll( function() {

      // Stick content nav and hide site nav
      if( $(this).scrollTop() > header_ht) {

        $nav.addClass(scroll_class).css('top', top_nav_ht);
        $('.header--single__hero__info').css('padding-top', nav_ht);

      // Unstick content nav, show site nav
      } else if( $(this).scrollTop() < header_ht ) {

        $nav.removeClass(scroll_class).css('top', '0');
        $('.header--single__hero__info').css('padding-top', '0');

      }

    }); // END scroll






    // ----
    // Conditionally Load Ad Scripts
    // ----

    // For Ad Block only

    var parValue = Cookies.get("par");
    var $hasTWC = $('a[href*="weather.com"]');

    if (parValue) {
      $hasTWC.each(function() {
        var currentUrl = $(this).attr('href'),
            newUrl = updateQueryStringParameter(currentUrl, "par", parValue);
        $(this).attr('href', newUrl);
      });
    }



















    // For Top Ad only

    $('.ad__wrap').each( function() {
      var adPlat,
          adSlot,
          adPos,
          adId = $(this).attr('id').toString(),
          adWidth = parseInt($(this).attr('data-size-w')),
          adHeight = parseInt($(this).attr('data-size-h'));
          // adPos = $(this).attr('data-pos').toString(),
          // adPar = Cookies.get("par").toString(),

      var adSlotOpts = {
        d : $(this).attr('data-slot-d').toString(),
        t : $(this).attr('data-slot-t').toString(),
        m : $(this).attr('data-slot-m').toString()
      };

      var adPlatOpts = {
        d : $(this).attr('data-plat-d').toString(),
        t : $(this).attr('data-plat-t').toString(),
        m : $(this).attr('data-plat-m').toString()
      };

      var adPosOpts = {
        d : $(this).attr('data-pos-d').toString(),
        t : $(this).attr('data-pos-t').toString(),
        m : $(this).attr('data-pos-m').toString()
      };

      var width = $(window).width();

      // Mobile
      if (width <= 767) {
        // console..log("window width = "+width+". Mobile ads displaying");
        // adHeight = adHeightOpts.m;
        // adWidth = adWidthOpts.m;
        adPlat = adPlatOpts.m;
        adSlot = adSlotOpts.m;
        adPos = adPosOpts.m;

      // Tablet
      } else if ((width >= 768) && (width <= 1024)){
        // console.log("window width = "+width+". Tablet ads displaying");
        // adHeight = adHeightOpts.t;
        // adWidth = adWidthOpts.t;
          adPlat = adPlatOpts.t;
          adSlot = adSlotOpts.t;
          adPos = adPosOpts.t;

      // Desktop
      } else {
        // console.log("window width = "+width+". Desktop ads displaying");
        // adHeight = adHeightOpts.d;
        // adWidth = adWidthidthOpts.d;
        adPlat = adPlatOpts.d;
        adSlot = adSlotOpts.d;
        adPos = adPosOpts.d;
      }

      googletag.cmd.push(function() {
        googletag.defineSlot(adSlot, [[adWidth, adHeight]], adId)
          // .setTargeting('par', [adPar])
          .addService(googletag.pubads())
          .setTargeting("pos", adPos)
          .setTargeting("plat", adPlat);
        googletag.enableServices();
        googletag.display(adId);

      });

    });




    // Old, simpler ad block
    $('.ad__wrap--old').each( function() {

      var adPos,
          adPlat,
          adSlot;

      var adId = $(this).attr('id'),
          parValue = Cookies.get("par");

      if( $('body').hasClass('twc-is-mobile') ) {
        adPos = $(this).attr('data-pos-m');
        adSlot = "/7646/mobile_smart_us/long_stories";
        adPlat = "wx_mw";
      } else {
        adPos = $(this).attr('data-pos-d');
        adSlot = "/7646/web_weather_us/long_stories";
        adPlat = "mx";
      }

      googletag.cmd.push(function() {
        googletag.defineSlot(adSlot, [[300, 251]], adId)
          .addService(googletag.pubads())
          .setTargeting("par", [parValue])
          .setTargeting("pos", adPos)
          .setTargeting("plat", adPlat);
        googletag.enableServices();
        googletag.display(adId);
      });
    });

}); // document.ready

// Draggable funtionality - credits to http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
function drags(dragElement, resizeElement, container, labelContainer, labelResizeElement) {
    dragElement.on("mousedown vmousedown", function(e) {
        dragElement.addClass('draggable');
        resizeElement.addClass('resizable');

        var dragWidth = dragElement.outerWidth(),
            xPosition = dragElement.offset().left + dragWidth - e.pageX,
            containerOffset = container.offset().left,
            containerWidth = container.outerWidth(),
            minLeft = containerOffset - 12,
            maxLeft = containerOffset + containerWidth - dragWidth + 22;

        dragElement.parents().on("mousemove vmousemove", function(e) {
            leftValue = e.pageX + xPosition - dragWidth;

            //constrain the draggable element to move inside his container
            if(leftValue < minLeft ) {
                leftValue = minLeft;
            } else if ( leftValue > maxLeft) {
                leftValue = maxLeft;
            }

            widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';

            $('.draggable').css('left', widthValue).on("mouseup vmouseup", function() {
                $(this).removeClass('draggable');
                resizeElement.removeClass('resizable');
            });

            $('.resizable').css('width', widthValue);

        }).on("mouseup vmouseup", function(e){
            dragElement.removeClass('draggable');
            resizeElement.removeClass('resizable');
        });
        e.preventDefault();
    }).on("mouseup vmouseup", function(e) {
        dragElement.removeClass('draggable');
        resizeElement.removeClass('resizable');
    });
}

// Append query string and use ? or & based on whether a query string is already present
// Credit: http://stackoverflow.com/questions/5999118/add-or-update-query-string-parameter
function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}
