// http://www.bobz.co/ajax-filter-posts-tag/

$(document).ready( function() {

  $('.cat-filter').click( function(event) {

        // Prevent defualt action - opening tag page
        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }

        // Get tag slug from title attirbute
        var selected_taxonomy = $(this).attr('title');

        $('.cat-filter').removeClass('selected');
        $(this).addClass('selected');

    });

  $('.cat-filter').click( function(event) {

    // Prevent defualt action - opening tag page
    if (event.preventDefault) {
      event.preventDefault();
    } else {
      event.returnValue = false;
    }

    // Get tag slug from title attirbute
    var selected_taxonomy = $(this).attr('title');

    $('.filtered-posts').fadeOut();

    data = {
      action: 'filter_posts',
      afp_nonce: afp_vars.afp_nonce,
      taxonomy: selected_taxonomy,
    };

    $.ajax({
      type: 'post',
      dataType: 'json',
      url: afp_vars.afp_ajax_url,
      data: data,
      beforeSend:function(){
        $('.chapters-loading').animate({'opacity' : '1', 'min-height' : '60vh'}, 300);
      },
      success: function( data, textStatus, XMLHttpRequest ) {
        $('.chapters-loading').animate({'opacity' : '0', 'min-height' : '0', 'max-height' : '0'}, 300);
        $('.filtered-posts').html( data.response );
        $('.filtered-posts').fadeIn();
      },
      error: function( MLHttpRequest, textStatus, errorThrown ) {
        $('.filtered-posts').html( 'No posts found' );
        $('.filtered-posts').fadeIn();
      }
    });

  });
});

